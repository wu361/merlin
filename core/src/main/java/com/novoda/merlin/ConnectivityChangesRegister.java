package com.novoda.merlin;

import ohos.net.NetManager;
import ohos.net.NetSpecifier;

/**
 * 网络状态变化监听注册
 *
 * @since 2021-04-13
 */
class ConnectivityChangesRegister {
    private final NetManager connectivityManager;
    private final ConnectivityChangeEventExtractor connectivityChangeEventExtractor;
    private ConnectivityCallbacks connectivityCallbacks;

    ConnectivityChangesRegister(NetManager connectivityManager,
                                ConnectivityChangeEventExtractor connectivityChangeEventExtractor) {
        this.connectivityManager = connectivityManager;
        this.connectivityChangeEventExtractor = connectivityChangeEventExtractor;
    }

    void register(MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier) {
        registerNetworkCallbacks(connectivityChangesNotifier);
    }

    private void registerNetworkCallbacks(MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier) {
        NetSpecifier.Builder builder = new NetSpecifier.Builder();
        connectivityManager.addNetStatusCallback(builder.build(), connectivityCallbacks(connectivityChangesNotifier));
    }

    private ConnectivityCallbacks connectivityCallbacks(
            MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier) {
        if (connectivityCallbacks == null) {
            connectivityCallbacks = new ConnectivityCallbacks(
                    connectivityChangesNotifier, connectivityChangeEventExtractor);
        }
        return connectivityCallbacks;
    }

    void unregister() {
        unregisterNetworkCallbacks();
    }

    private void unregisterNetworkCallbacks() {
        connectivityManager.removeNetStatusCallback(connectivityCallbacks);
    }
}
