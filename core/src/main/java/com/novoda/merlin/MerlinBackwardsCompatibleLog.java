package com.novoda.merlin;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * MerlinBackwardsCompatibleLog
 *
 * @since 2021-04-13
 */
final class MerlinBackwardsCompatibleLog implements Logger.LogHandle {
    private static final HiLogLabel TAG = new HiLogLabel(3, 0xD001100, "Merlin");
    private static MerlinBackwardsCompatibleLog lazyInstance;

    private MerlinBackwardsCompatibleLog() {
        // Single instance.
    }

    static MerlinBackwardsCompatibleLog getInstance() {
        if (lazyInstance == null) {
            lazyInstance = new MerlinBackwardsCompatibleLog();
        }
        return lazyInstance;
    }

    @Override
    public void v(Object... message) {
        HiLog.fatal(TAG, message[0].toString());
    }

    @Override
    public void i(Object... message) {
        HiLog.info(TAG, message[0].toString());
    }

    @Override
    public void d(Object... msg) {
        HiLog.debug(TAG, msg[0].toString());
    }

    @Override
    public void d(Throwable throwable, Object... message) {
        HiLog.debug(TAG, message[0].toString(), throwable);
    }

    @Override
    public void w(Object... message) {
        HiLog.warn(TAG, message[0].toString());
    }

    @Override
    public void w(Throwable throwable, Object... message) {
        HiLog.warn(TAG, message[0].toString(), throwable);
    }

    @Override
    public void e(Object... message) {
        HiLog.error(TAG, message[0].toString());
    }

    @Override
    public void e(Throwable throwable, Object... message) {
        HiLog.error(TAG, message[0].toString(), throwable);
    }
}
