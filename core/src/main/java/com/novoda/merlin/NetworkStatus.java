package com.novoda.merlin;

/**
 * NetworkStatus
 *
 * @since 2021-04-13
 */
public class NetworkStatus {
    private final State state;

    /**
     * State
     *
     * @since 2021-04-13
     */
    private enum State {
        AVAILABLE,
        UNAVAILABLE
    }

    private NetworkStatus(State state) {
        this.state = state;
    }

    /**
     * newAvailableInstance
     *
     * @return NetworkStatus
     */
    public static NetworkStatus newAvailableInstance() {
        return new NetworkStatus(State.AVAILABLE);
    }

    /**
     * newUnavailableInstance
     *
     * @return NetworkStatus
     */
    public static NetworkStatus newUnavailableInstance() {
        return new NetworkStatus(State.UNAVAILABLE);
    }

    public boolean isAvailable() {
        return state.equals(State.AVAILABLE);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        NetworkStatus that = (NetworkStatus) object;
        return state == that.state;
    }

    @Override
    public int hashCode() {
        return state != null ? state.hashCode() : 0;
    }
}
