package com.novoda.merlin;

import java.util.List;

/**
 * MerlinCallbackManager
 *
 * @param <T>
 * @since 2021-04-13
 */
class MerlinCallbackManager<T extends Registerable> {
    private final Register<T> register;

    MerlinCallbackManager(Register<T> register) {
        this.register = register;
    }

    List<T> registerables() {
        return register.registerables();
    }
}
