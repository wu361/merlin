package com.novoda.merlin;

/**
 * Merlin
 *
 * @since 2021-04-13
 */
public class Merlin {
    private final MerlinServiceBinder merlinServiceBinder;
    private final Registrar registrar;

    Merlin(MerlinServiceBinder merlinServiceBinder, Registrar registrar) {
        this.merlinServiceBinder = merlinServiceBinder;
        this.registrar = registrar;
    }

    /**
     * setEndpoint
     *
     * @param endpoint
     * @param validator
     */
    public void setEndpoint(Endpoint endpoint, ResponseCodeValidator validator) {
        merlinServiceBinder.setEndpoint(endpoint, validator);
    }

    /**
     * bind
     */
    public void bind() {
        merlinServiceBinder.bindService();
    }

    /**
     * unbind
     */
    public void unbind() {
        merlinServiceBinder.unbind();
        registrar.clearRegistrations();
    }

    /**
     * registerConnectable
     *
     * @param connectable
     */
    public void registerConnectable(Connectable connectable) {
        registrar.registerConnectable(connectable);
    }

    /**
     * registerDisconnectable
     *
     * @param disconnectable
     */
    public void registerDisconnectable(Disconnectable disconnectable) {
        registrar.registerDisconnectable(disconnectable);
    }

    /**
     * registerBindable
     *
     * @param bindable
     */
    public void registerBindable(Bindable bindable) {
        registrar.registerBindable(bindable);
    }

    /**
     * MerlinBuilder
     *
     * @since 2021-04-13
     */
    public static class Builder extends MerlinBuilder {
    }
}
