package com.novoda.merlin;

/**
 * 网络状态绑定回调
 *
 * @since 2021-04-13
 */
public interface Bindable extends Registerable {
    /**
     * 已绑定回调
     *
     * @param networkStatus
     */
    void onBind(NetworkStatus networkStatus);
}