package com.novoda.merlin;

/**
 * 可连接回调
 *
 * @since 2021-04-13
 */
public interface Connectable extends Registerable {
    /**
     * 已连接回调
     */
    void onConnect();
}
