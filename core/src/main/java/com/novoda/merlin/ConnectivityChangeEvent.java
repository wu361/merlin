package com.novoda.merlin;

import java.util.Objects;

/**
 * 网络状态变化处理
 *
 * @since 2021-04-13
 */
class ConnectivityChangeEvent {
    private static final boolean IS_WITHOUT_CONNECTION = false;
    private static final String WITHOUT_REASON = "";
    private static final String WITHOUT_INFO = "";

    private final boolean isConnected;
    private final String info;
    private final String reason;

    private ConnectivityChangeEvent(boolean isConnected, String info, String reason) {
        this.isConnected = isConnected;
        this.info = info;
        this.reason = reason;
    }

    static ConnectivityChangeEvent createWithoutConnection() {
        return new ConnectivityChangeEvent(IS_WITHOUT_CONNECTION, WITHOUT_INFO, WITHOUT_REASON);
    }

    static ConnectivityChangeEvent createWithNetworkInfoChangeEvent(boolean isConnected, String info, String reason) {
        return new ConnectivityChangeEvent(isConnected, info, reason);
    }

    private boolean isConnected() {
        return isConnected;
    }

    String info() {
        return info;
    }

    String reason() {
        return reason;
    }

    NetworkStatus asNetworkStatus() {
        return isConnected() ? NetworkStatus.newAvailableInstance() : NetworkStatus.newUnavailableInstance();
    }

    @Override
    public String toString() {
        return "ConnectivityChangeEvent{"
                + "isConnected=" + isConnected
                + ", info='" + info + '\''
                + ", reason='" + reason + '\''
                + '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        ConnectivityChangeEvent that = (ConnectivityChangeEvent) object;
        if (isConnected != that.isConnected) {
            return false;
        }
        if (!Objects.equals(info, that.info)) {
            return false;
        }
        return Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        int result = isConnected ? 1 : 0;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        return result;
    }
}
