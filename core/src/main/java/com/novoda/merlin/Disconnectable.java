package com.novoda.merlin;

/**
 * 不可连接回调
 *
 * @since 2021-04-13
 */
public interface Disconnectable extends Registerable {
    /**
     * 未连接回调
     */
    void onDisconnect();
}
