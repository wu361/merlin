package com.novoda.merlin;

import ohos.net.NetHandle;
import ohos.net.NetStatusCallback;

/**
 * 网络状态变化回调
 *
 * @since 2021-04-13
 */
class ConnectivityCallbacks extends NetStatusCallback {
    private final MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier;
    private final ConnectivityChangeEventExtractor connectivityChangeEventExtractor;

    ConnectivityCallbacks(MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier,
                          ConnectivityChangeEventExtractor connectivityChangeEventExtractor) {
        this.connectivityChangesNotifier = connectivityChangesNotifier;
        this.connectivityChangeEventExtractor = connectivityChangeEventExtractor;
    }

    @Override
    public void onAvailable(NetHandle netHandle) {
        notifyMerlinService(netHandle);
    }

    @Override
    public void onLosing(NetHandle netHandle, long maxMsToLive) {
        notifyMerlinService(netHandle);
    }

    @Override
    public void onLost(NetHandle netHandle) {
        notifyMerlinService(netHandle);
    }

    private void notifyMerlinService(NetHandle netHandle) {
        if (!connectivityChangesNotifier.canNotify()) {
            Logger.d("Cannot notify " + MerlinService.ConnectivityChangesNotifier.class.getSimpleName());
            return;
        }
        ConnectivityChangeEvent connectivityChangeEvent = connectivityChangeEventExtractor.extractFrom(netHandle);
        connectivityChangesNotifier.notify(connectivityChangeEvent);
    }
}
