package com.novoda.merlin;

import ohos.app.Context;

/**
 * This class provides a mechanism for retrieving the current
 * state of a network connection given an application context.
 *
 * @since 2021-04-13
 */
public class MerlinsBeard {
    private static final boolean IS_NOT_CONNECTED_TO_NETWORK_TYPE = false;

    private final EndpointPinger captivePortalPinger;
    private final Ping captivePortalPing;

    private final NetworkInfo networkInfo;

    MerlinsBeard(EndpointPinger captivePortalPinger, Ping captivePortalPing,NetworkInfo networkInfo) {
        this.captivePortalPinger = captivePortalPinger;
        this.captivePortalPing = captivePortalPing;
        this.networkInfo = networkInfo;
    }

    /**
     * MerlinsBeard from
     *
     * @deprecated Use {@link Builder} instead.
     *
     * Use this method to create a MerlinsBeard object, this is how you can retrieve the current network state.
     *
     * @param context pass any context application or activity.
     * @return MerlinsBeard.
     */
    @Deprecated
    public static MerlinsBeard from(Context context) {
        return new Builder()
                .build(context);
    }

    /**
     * Provides a boolean representing whether a network connection has been established.
     * NOTE: Therefore available does not necessarily mean that an internet connection
     * is available.
     *
     * @return boolean true if a network connection is available.
     */
    public boolean isConnected() {
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     * Provides a boolean representing whether a mobile network connection has been established and is active.
     * <p>
     * NOTE: Therefore available does not necessarily mean that an internet connection
     * is available. Also, there can be only one network connection at a time, so this would return false if
     * the active connection is the Wi-Fi one, even if there is a (inactive) mobile network connection established.
     * </p>
     *
     * @return boolean true if a mobile network connection is available.
     */
    public boolean isConnectedToMobileNetwork() {
        return isConnectedTo(NetworkInfo.TYPE_MOBILE);
    }

    /**
     * Provides a boolean representing whether a Wi-Fi network connection has been established.
     * <p>
     * NOTE: Therefore available does not necessarily mean that an internet connection
     * is available.
     * </p>
     *
     * @return boolean true if a Wi-Fi network connection is available.
     */
    public boolean isConnectedToWifi() {
        return isConnectedTo(NetworkInfo.TYPE_WIFI);
    }

    private boolean isConnectedTo(int networkType) {
        return networkInfo != null && networkInfo.getNetworkInfo(networkType);
    }

    /**
     * Provides a human-readable String describing the network subtype (e.g. UMTS, LTE)
     * when connected to a mobile network.
     *
     * @return network subtype name, or empty string if not connected to a mobile network.
     */
    public String getMobileNetworkSubtypeName() {
        if (networkInfo == null || !networkInfo.isConnected() || isConnectedToWifi()) {
            return "";
        }
        return networkInfo.getSubtypeName();
    }

    /**
     * Detects if a client has internet access by pinging an {@link Endpoint}.
     *
     * @param callback to call with boolean result representing if a client has internet access.
     */
    public void hasInternetAccess(final InternetAccessCallback callback) {
        if (networkInfo != null && networkInfo.isConnected()) {
            captivePortalPinger.ping(new EndpointPinger.PingerCallback() {
                @Override
                public void onSuccess() {
                    callback.onResult(true);
                }

                @Override
                public void onFailure() {
                    callback.onResult(false);
                }
            });
        } else {
            callback.onResult(false);
        }
    }

    /**
     * Synchronously detects if a client has internet access by pinging an {@link Endpoint}.
     * Clients are expected to handle their own threading.
     *
     * @return Boolean result representing if a client has internet access.
     */
    public boolean hasInternetAccess() {
        return captivePortalPing.doSynchronousPing();
    }

    /**
     * InternetAccessCallback
     *
     * @since 2021-04-13
     */
    public interface InternetAccessCallback {
        /**
         * onResult
         *
         * @param isHasAccess
         */
        void onResult(boolean isHasAccess);
    }

    /**
     * Builder
     *
     * @since 2021-04-13
     */
    public static class Builder extends MerlinsBeardBuilder {
    }
}
