package com.novoda.merlin;

/**
 * RequestMaker
 *
 * @since 2021-04-13
 */
interface RequestMaker {
    Request head(Endpoint endpoint);
}
