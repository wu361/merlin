package com.novoda.merlin;

/**
 * 网络监听服务绑定回调
 *
 * @since 2021-04-13
 */
class BindCallbackManager extends MerlinCallbackManager<Bindable> {
    BindCallbackManager(Register<Bindable> register) {
        super(register);
    }

    void onMerlinBind(NetworkStatus networkStatus) {
        Logger.d("onBind");
        for (Bindable bindable : registerables()) {
            bindable.onBind(networkStatus);
        }
    }
}
