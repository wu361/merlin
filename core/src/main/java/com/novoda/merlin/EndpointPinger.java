package com.novoda.merlin;

/**
 * 网络状态请求接口
 *
 * @since 2021-04-13
 */
class EndpointPinger {
    private final Endpoint endpoint;
    private final PingTaskFactory pingTaskFactory;

    EndpointPinger(Endpoint endpoint, PingTaskFactory pingTaskFactory) {
        this.endpoint = endpoint;
        this.pingTaskFactory = pingTaskFactory;
    }

    /**
     * 网络状态请求接口回调
     *
     * @since 2021-04-13
     */
    interface PingerCallback {
        void onSuccess();

        void onFailure();
    }

    static EndpointPinger withCustomEndpointAndValidation(Endpoint endpoint, ResponseCodeValidator validator) {
        PingTaskFactory pingTaskFactory = new PingTaskFactory(new ResponseCodeFetcher(), validator);
        return new EndpointPinger(endpoint, pingTaskFactory);
    }

    void ping(PingerCallback pingerCallback) {
        PingTask pingTask = pingTaskFactory.create(endpoint, pingerCallback);
        new Thread(pingTask).start();
    }

    void noNetworkToPing(PingerCallback pingerCallback) {
        pingerCallback.onFailure();
    }

    /**
     * 网络状态请求接口返回code
     *
     * @since 2021-04-13
     */
    static class ResponseCodeFetcher {
        public int from(Endpoint endpoint) {
            return MerlinRequest.head(endpoint).getResponseCode();
        }
    }
}
