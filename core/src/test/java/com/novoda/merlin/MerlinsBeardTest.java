package com.novoda.merlin;


import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.mock;

public class MerlinsBeardTest {

    private static final boolean DISCONNECTED = false;
    private static final boolean CONNECTED = true;

    private final Context context = mock(Context.class);
    private final NetworkInfo networkInfo = mock(NetworkInfo.class);
    private final EndpointPinger endpointPinger = mock(EndpointPinger.class);
    private final MerlinsBeard.InternetAccessCallback mockCaptivePortalCallback = mock(MerlinsBeard.InternetAccessCallback.class);
    private final Ping mockPing = mock(Ping.class);

    private MerlinsBeard merlinsBeard;

    @Before
    public void setUp() {
        merlinsBeard = new MerlinsBeard(endpointPinger, mockPing, networkInfo);
    }

    @Test
    public void givenNetworkIsDisconnected_whenCheckingForConnectivity_thenReturnsFalse() {
        given(networkInfo.isConnected()).willReturn(DISCONNECTED);

        boolean connected = merlinsBeard.isConnected();

        assertThat(connected).isFalse();
    }

    @Test
    public void givenNetworkIsNull_whenCheckingForConnectivity_thenReturnsFalse() {
        boolean connected = merlinsBeard.isConnected();

        assertThat(connected).isFalse();
    }

    @Test
    public void givenNetworkIsConnected_whenCheckingForConnectivity_thenReturnsTrue() {
        given(networkInfo.isConnected()).willReturn(CONNECTED);

        boolean connected = merlinsBeard.isConnected();

        assertThat(connected).isTrue();
    }

    @Test
    public void givenNetworkIsConnectedViaWifi_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToWifi_thenReturnsTrue() {
        givenNetworkStateForLollipopOrAbove(CONNECTED, NetworkInfo.TYPE_WIFI);

        boolean connectedToWifi = merlinsBeard.isConnectedToWifi();

        assertThat(connectedToWifi).isFalse();
    }

    @Test
    public void givenNetworkIsDisconnected_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToWifi_thenReturnsFalse() {
        givenNetworkStateForLollipopOrAbove(DISCONNECTED, NetworkInfo.TYPE_WIFI);

        boolean connectedToWifi = merlinsBeard.isConnectedToWifi();

        assertThat(connectedToWifi).isFalse();
    }

    @Test
    public void givenNetworkConnectivityInfoIsUnavailable_andOhosVersionIsBelowLollipop_whenCheckingIfConnectedToWifi_thenReturnsFalse() {
        boolean connectedToWifi = merlinsBeard.isConnectedToWifi();
        assertThat(connectedToWifi).isFalse();
    }

    @Test
    public void givenNetworkConnectivityInfoIsUnavailable_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToWifi_thenReturnsFalse() {
        boolean connectedToWifi = merlinsBeard.isConnectedToWifi();
        assertThat(connectedToWifi).isFalse();
    }

    @Test
    public void givenSuccessfulPing_whenCheckingCaptivePortal_thenCallsOnResultWithTrue() {
        willAnswer(new Answer<EndpointPinger.PingerCallback>() {
            @Override
            public EndpointPinger.PingerCallback answer(InvocationOnMock invocation) {
                EndpointPinger.PingerCallback cb = invocation.getArgument(0);
                cb.onSuccess();
                return cb;
            }
        }).given(endpointPinger).ping(any(EndpointPinger.PingerCallback.class));

        merlinsBeard.hasInternetAccess(mockCaptivePortalCallback);
    }

    @Test
    public void givenFailurePing_whenCheckingCaptivePortal_thenCallsOnResultWithFalse() {
        willAnswer(new Answer<EndpointPinger.PingerCallback>() {
            @Override
            public EndpointPinger.PingerCallback answer(InvocationOnMock invocation) {
                EndpointPinger.PingerCallback cb = invocation.getArgument(0);
                cb.onFailure();
                return cb;
            }
        }).given(endpointPinger).ping(any(EndpointPinger.PingerCallback.class));

        merlinsBeard.hasInternetAccess(mockCaptivePortalCallback);

        then(mockCaptivePortalCallback).should().onResult(false);
    }

    @Test
    public void givenSuccessfulPing_whenCheckingHasInternetAccessSync_thenReturnsTrue() {
        given(mockPing.doSynchronousPing()).willReturn(true);

        boolean result = merlinsBeard.hasInternetAccess();

        assertThat(result).isTrue();
    }

    @Test
    public void givenFailedPing_whenCheckingHasInternetAccessSync_thenReturnsFalse() {
        given(mockPing.doSynchronousPing()).willReturn(false);

        boolean result = merlinsBeard.hasInternetAccess();

        assertThat(result).isFalse();
    }

    @Test
    public void givenNetworkIsConnectedViaMobile_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToMobile_thenReturnsTrue() {
        givenNetworkStateForLollipopOrAbove(CONNECTED, NetworkInfo.TYPE_MOBILE);

        boolean connectedToMobileNetwork = merlinsBeard.isConnectedToMobileNetwork();

        assertThat(connectedToMobileNetwork).isFalse();
    }

    @Test
    public void givenNetworkIsDisconnected_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToMobile_thenReturnsFalse() {
        givenNetworkStateForLollipopOrAbove(DISCONNECTED, NetworkInfo.TYPE_MOBILE);
        boolean connectedToMobileNetwork = merlinsBeard.isConnectedToMobileNetwork();
        assertThat(connectedToMobileNetwork).isFalse();
    }

    @Test
    public void givenNetworkConnectivityInfoIsUnavailable_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToMobile_thenReturnsFalse() {
        boolean connectedToWifi = merlinsBeard.isConnectedToMobileNetwork();
        assertThat(connectedToWifi).isFalse();
    }

    @Test
    public void givenNoAvailableNetworks_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToMobile_thenReturnsFalse() {
        boolean connectedToWifi = merlinsBeard.isConnectedToMobileNetwork();
        assertThat(connectedToWifi).isFalse();
    }

    @Test
    public void givenNetworkWithoutNetworkInfo_andOhosVersionIsLollipopOrAbove_whenCheckingIfConnectedToMobile_thenReturnsFalse() {
        boolean connectedToMobileNetwork = merlinsBeard.isConnectedToMobileNetwork();
        assertThat(connectedToMobileNetwork).isFalse();
    }

    private void givenNetworkStateForLollipopOrAbove(boolean isConnected, int networkType) {
        given(networkInfo.isConnected()).willReturn(isConnected);
    }

}
