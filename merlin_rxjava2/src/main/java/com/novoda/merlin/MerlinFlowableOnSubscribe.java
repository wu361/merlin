package com.novoda.merlin;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Cancellable;

/**
 * MerlinFlowableOnSubscribe
 *
 * @since 2021-04-13
 */
class MerlinFlowableOnSubscribe implements FlowableOnSubscribe<NetworkStatus> {
    private Merlin merlin;

    MerlinFlowableOnSubscribe(Merlin merlin) {
        this.merlin = merlin;
    }

    @Override
    public void subscribe(@NonNull FlowableEmitter<NetworkStatus> emitter) {
        merlin.registerConnectable(createConnectable(emitter));
        merlin.registerDisconnectable(createDisconnectable(emitter));
        merlin.registerBindable(createBindable(emitter));

        emitter.setCancellable(createCancellable());

        merlin.bind();
    }

    private Connectable createConnectable(final FlowableEmitter<NetworkStatus> emitter) {
        return () -> emitter.onNext(NetworkStatus.newAvailableInstance());
    }

    private Disconnectable createDisconnectable(final FlowableEmitter<NetworkStatus> emitter) {
        return () -> emitter.onNext(NetworkStatus.newUnavailableInstance());
    }

    private Bindable createBindable(final FlowableEmitter<NetworkStatus> emitter) {
        return current -> emitter.onNext(current);
    }

    private Cancellable createCancellable() {
        return () -> merlin.unbind();
    }
}
