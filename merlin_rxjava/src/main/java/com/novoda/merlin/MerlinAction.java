package com.novoda.merlin;

import rx.Emitter;
import rx.functions.Action1;
import rx.functions.Cancellable;

/**
 * MerlinAction
 *
 * @since 2021-04-13
 */
class MerlinAction implements Action1<Emitter<NetworkStatus>> {
    private Merlin merlin;

    MerlinAction(Merlin merlin) {
        this.merlin = merlin;
    }

    @Override
    public void call(Emitter<NetworkStatus> stateEmitter) {
        merlin.registerConnectable(createConnectable(stateEmitter));
        merlin.registerDisconnectable(createDisconnectable(stateEmitter));
        merlin.registerBindable(createBindable(stateEmitter));

        stateEmitter.setCancellation(createCancellable());

        merlin.bind();
    }

    private Connectable createConnectable(final Emitter<NetworkStatus> stateEmitter) {
        return () -> stateEmitter.onNext(NetworkStatus.newAvailableInstance());
    }

    private Disconnectable createDisconnectable(final Emitter<NetworkStatus> stateEmitter) {
        return () -> stateEmitter.onNext(NetworkStatus.newUnavailableInstance());
    }

    private Bindable createBindable(final Emitter<NetworkStatus> stateEmitter) {
        return current -> stateEmitter.onNext(current);
    }

    private Cancellable createCancellable() {
        return () -> merlin.unbind();
    }
}
