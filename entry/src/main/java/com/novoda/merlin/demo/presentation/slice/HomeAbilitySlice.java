package com.novoda.merlin.demo.presentation.slice;

import com.novoda.merlin.demo.ResourceTable;
import com.novoda.merlin.demo.view.MaterialRippleLayout;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * HomeAbilitySlice
 *
 * @since 2021-04-13
 */
public class HomeAbilitySlice extends AbilitySlice {
    private static final String BUNDLE_NAME = "com.novoda.merlin.demo";
    private static final int TITLE_COLOR = Color.getIntColor("#808080");
    private static final float RIPPLE_ALPHA = 0.2f;
    private static final int RIPPLE_DURATION = 2000;
    private static final int RIPPLE_DIAMETERDP = 35;
    private MaterialRippleLayout materialRippleLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_home);
        Text title = (Text) findComponentById(ResourceTable.Id_tx_title);
        title.setText(ResourceTable.String_merlin_demo);

        Button demoLaunchBtn = (Button) findComponentById(ResourceTable.Id_demo_launch_button);
        startAnimator(demoLaunchBtn);
        materialRippleLayout.setOnClick(() -> {
            Intent mIntent = new Intent();
            Operation mOperation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(BUNDLE_NAME)
                    .withAbilityName("com.novoda.merlin.demo.presentation.DemoAbility")
                    .build();
            mIntent.setOperation(mOperation);
            startAbility(mIntent);
        });

        Button rjDemoLaunchBtn = (Button) findComponentById(ResourceTable.Id_rx_java_demo_launch_button);
        startAnimator(rjDemoLaunchBtn);
        materialRippleLayout.setOnClick(() -> {
            Intent mIntent = new Intent();
            Operation mOperation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(BUNDLE_NAME)
                    .withAbilityName("com.novoda.merlin.demo.presentation.RxJavaDemoActivity")
                    .build();
            mIntent.setOperation(mOperation);
            startAbility(mIntent);
        });

        Button rj2DemoLaunchBtn = (Button) findComponentById(ResourceTable.Id_rx_java2_demo_launch_button);
        startAnimator(rj2DemoLaunchBtn);
        materialRippleLayout.setOnClick(() -> {
            Intent mIntent = new Intent();
            Operation mOperation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(BUNDLE_NAME)
                    .withAbilityName("com.novoda.merlin.demo.presentation.RxJava2DemoActivity")
                    .build();
            mIntent.setOperation(mOperation);
            startAbility(mIntent);
        });
    }

    /**
     * 启动动画
     *
     * @param bt
     */
    private void startAnimator(Button bt) {
        if (bt != null) {
            materialRippleLayout = new MaterialRippleLayout(this).on(bt)
                    .rippleColor(Color.getIntColor("#86666666"))
                    .rippleAlpha(RIPPLE_ALPHA)
                    .rippleDuration(RIPPLE_DURATION)
                    .rippleBackground(Color.getIntColor("#D1D1D1"))
                    .rippleOverlay(true)
                    .rippleDiameterDp(RIPPLE_DIAMETERDP)
                    .create();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
