package com.novoda.merlin.demo.connectivity.display;

import com.novoda.merlin.MerlinsBeard;
import com.novoda.merlin.demo.ResourceTable;

import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

/**
 * NetworkStatusDisplayer
 *
 * @since 2021-04-13
 */
public class NetworkStatusDisplayer {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "MerlinAbilitySlice");
    private Context context;
    private final MerlinsBeard merlinsBeard;

    private MerlinToastDialog toastDialog;

    /**
     * NetworkStatusDisplayer
     *
     * @param context
     * @param merlinsBeard
     */
    public NetworkStatusDisplayer(Context context, MerlinsBeard merlinsBeard) {
        this.context = context;
        this.merlinsBeard = merlinsBeard;
    }

    private String getmessageResourceString(int messageResource) {
        ohos.global.resource.ResourceManager resManager = context.getResourceManager();
        try {
            return resManager.getElement(messageResource).getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            HiLog.error(LABEL_LOG,"getmessageResourceString(int messageResource) e");
        }
        return "";
    }

    /**
     * displayPositiveMessage
     *
     * @param messageResource
     */
    public void displayPositiveMessage(int messageResource) {
        PositiveLayout positiveLayout = new PositiveLayout(context);
        positiveLayout.setText(getmessageResourceString(messageResource));
        toastDialog = MerlinToastDialog.withDuration(context, ResourceTable.Integer_snack_bar_duration)
                .withLayout(positiveLayout)
                .show();
    }

    /**
     * displayNegativeMessage
     *
     * @param messageResource
     */
    public void displayNegativeMessage(int messageResource) {
        NegativeLayout negativeLayout = new NegativeLayout(context);
        negativeLayout.setText(getmessageResourceString(messageResource));
        toastDialog = MerlinToastDialog.withDuration(context, ResourceTable.Integer_snack_bar_duration)
                .withLayout(negativeLayout)
                .show();
    }

    /**
     * displayNegativeMessage
     */
    public void displayNetworkSubtype() {
        String subtype = merlinsBeard.getMobileNetworkSubtypeName();
        toastDialog = MerlinToastDialog.withDuration(context, ResourceTable.Integer_snack_bar_duration)
                .withLayout(subtypeThemerFrom(subtype))
                .show();
    }

    private Component subtypeThemerFrom(String subtype) {
        if (subtypeAbsent(subtype)) {
            NegativeLayout negativeLayout = new NegativeLayout(context);
            negativeLayout.setText(getmessageResourceString(ResourceTable.String_subtype_not_available));
            return negativeLayout;
        } else {
            PositiveLayout positiveLayout = new PositiveLayout(context);
            positiveLayout.setText(getmessageResourceString(ResourceTable.String_subtype_value) + subtype);
            return positiveLayout;
        }
    }

    private boolean subtypeAbsent(String subtype) {
        return subtype == null || subtype.isEmpty();
    }

    /**
     * reset
     */
    public void reset() {
        if (toastDialog == null) {
            return;
        }
        toastDialog.dismiss();
        toastDialog = null;
    }
}
