package com.novoda.merlin.demo.connectivity.display;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

/**
 * MerlinToastDialog
 *
 * @since 2021-04-13
 */
class MerlinToastDialog {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "MerlinToastDialog");
    private final ToastDialog toastDialog;

    private MerlinToastDialog(ToastDialog toastDialog) {
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        this.toastDialog = toastDialog;
    }

    static MerlinToastDialog withDuration(Context context, int durationResource) {
        ohos.global.resource.ResourceManager resManager = context.getResourceManager();
        ToastDialog toastDialog = new ToastDialog(context);
        try {
            int duration = resManager.getElement(durationResource).getInteger();
            toastDialog.setDuration(duration);
        } catch (IOException | NotExistException | WrongTypeException e) {
            HiLog.error(LABEL_LOG,"MerlinToastDialog withDuration(Context context, int durationResource) e");
        }
        return new MerlinToastDialog(toastDialog);
    }

    MerlinToastDialog withLayout(Component component) {
        toastDialog.setComponent(component);
        return this;
    }

    MerlinToastDialog withText(String message) {
        toastDialog.setText(message);
        return this;
    }

    MerlinToastDialog withText(int messageResource) {
        ohos.global.resource.ResourceManager resManager = toastDialog.getComponent().getContext().getResourceManager();
        try {
            String result = resManager.getElement(messageResource).getString();
            toastDialog.setText(result);
        } catch (IOException | NotExistException | WrongTypeException e) {
            HiLog.error(LABEL_LOG,"MerlinToastDialog withText(int messageResource) e");
        }

        return this;
    }

    MerlinToastDialog show() {
        if (!toastDialog.isShowing()) {
            toastDialog.show();
        }
        return this;
    }

    void dismiss() {
        toastDialog.cancel();
    }
}
