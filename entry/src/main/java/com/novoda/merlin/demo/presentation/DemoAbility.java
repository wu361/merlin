package com.novoda.merlin.demo.presentation;

import com.novoda.merlin.demo.presentation.slice.DemoAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * DemoAbility
 *
 * @since 2021-04-13
 */
public class DemoAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DemoAbilitySlice.class.getName());
    }
}
