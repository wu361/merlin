package com.novoda.merlin.demo.presentation;

import com.novoda.merlin.demo.presentation.slice.RxJava2DemoActivitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * RxJava2DemoActivity
 *
 * @since 2021-04-13
 */
public class RxJava2DemoActivity extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RxJava2DemoActivitySlice.class.getName());
    }
}
